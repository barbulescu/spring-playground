package com.barbulescu.service2;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
public class Service2Application {

    public static void main(String[] args) {
        SpringApplication.run(Service2Application.class, args);
    }

}

@RestController
@Log4j2
class HelloController {

    private final String name1;

    HelloController(@Value("${name1}") String name1) {
        this.name1 = name1;
        log.info("-------> {}", name1);
    }

    @GetMapping("/hello/{name}")
    @LoadBalanced
    public String hello(@PathVariable("name") String name) {
        return String.format("Hello %s!", name);
    }
}
