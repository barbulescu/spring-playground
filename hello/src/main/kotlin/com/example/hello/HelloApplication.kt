package com.example.hello

import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Condition
import org.springframework.context.annotation.ConditionContext
import org.springframework.core.type.AnnotatedTypeMetadata
import org.springframework.stereotype.Component

@SpringBootApplication
@EnableConfigurationProperties(TaskProperties::class)
class HelloApplication(val tasks: List<Task>) : ApplicationRunner {
    override fun run(args: ApplicationArguments?) {
        tasks.forEach { it.sayYourName() }
    }

}

fun main(args: Array<String>) {
    runApplication<HelloApplication>(*args)
}

@Component
@ConditionalOnExpression(
    "\${all} or '\${tasks}'.contains('t1')"
)
class Task1 : Task {
    override fun sayYourName() {
        println("Task1")
    }

}

@Component
@ConditionalOnExpression(
	"\${all} or '\${tasks}'.contains('t2')"
)
//@Conditional(CustomCondition::class)
class Task2 : Task {
    override fun sayYourName() {
        println("Task2")
    }
}

interface Task {
    fun sayYourName()
}

@Component
class CustomCondition : Condition {
    override fun matches(context: ConditionContext, metadata: AnnotatedTypeMetadata): Boolean {
		val bean = context.beanFactory?.getBean(TaskProperties::class.java)
		return bean?.enabled("t2") ?: false
    }
}

@ConfigurationProperties(prefix = "tasks")
data class TaskProperties(private var all: Boolean, private var tasks: List<String>) {
    fun enabled(task: String): Boolean = all || tasks.contains(task)
}
