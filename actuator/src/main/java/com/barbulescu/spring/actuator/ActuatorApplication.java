package com.barbulescu.spring.actuator;

import io.micrometer.core.instrument.Measurement;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toUnmodifiableList;

@RequiredArgsConstructor
@SpringBootApplication
public class ActuatorApplication implements ApplicationRunner {

    public static void main(String[] args) {
        SpringApplication.run(ActuatorApplication.class, args);
    }

    private final HelloProcessor helloProcessor;
    private final MeterRegistry meterRegistry;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        helloProcessor.sayHello("World");
        helloProcessor.sayHello("Actuator");


        helloProcessor.sayHello(List.of("1", "2"));
        helloProcessor.sayHello(List.of("3", "4", "5"));
        helloProcessor.sayHello(IntStream.range(0, 100).mapToObj(String::valueOf).collect(toUnmodifiableList()));

        meterRegistry.getMeters().stream()
                .filter(meter -> meter.getId().getName().startsWith("hello"))
                .forEach(meter -> {

                    System.out.println();
                    System.out.println(meter.getId());
                    for (Measurement measurement : meter.measure()) {
                        System.out.println(measurement.getStatistic().getTagValueRepresentation() + "=" + measurement.getValue());
                    }
                });
    }
}
