package com.barbulescu.spring.actuator;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class HelloProcessor {
    private final Timer timer;
    private final DistributionSummary summary;
    private final Counter counter;

    public HelloProcessor(MeterRegistry registry) {
        timer = registry.timer("hello.timer");
        summary = DistributionSummary
                .builder("hello.summary")
                .publishPercentiles(0.3, 0.5, 0.95)
                .publishPercentileHistogram(true)
                .register(registry);

        counter = Counter
                .builder("hello.counter")
                .register(registry);
    }

    public void sayHello(String name) {
        timer.record(() -> System.out.println("Hello " + name));
    }

    public void sayHello(Collection<String> names) {
        System.out.println("Hello all");
        summary.record(names.size());
        counter.increment(names.size());
    }
}
