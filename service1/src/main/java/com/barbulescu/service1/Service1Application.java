package com.barbulescu.service1;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
public class Service1Application {
    public static void main(String[] args) {
        SpringApplication.run(Service1Application.class, args);
    }
}

@FeignClient(name = "hello")
interface HelloService {
    @GetMapping(value = "/hello/{name}")
    String hello(@PathVariable("name") String name);
}

@Component
@Log4j2
class Service2Caller implements ApplicationRunner {
    private final HelloService helloService;
    private final String name1;

    Service2Caller(HelloService helloService, @Value("${name1}") String name1) {
        this.helloService = helloService;
        this.name1 = name1;
    }

    @Override
    public void run(ApplicationArguments args) {
        log.info("----> {}", name1);
        String helloWorld = helloService.hello("World");
        log.info("First response = {}", helloWorld);
        String helloUniverse = helloService.hello("Universe");
        log.info("Second response = {}", helloUniverse);
    }
}
