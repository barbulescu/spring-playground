package com.barbulescu.spring.elasticsearch;

import com.barbulescu.spring.elasticsearch.data.Department;
import com.barbulescu.spring.elasticsearch.data.Employee;
import com.barbulescu.spring.elasticsearch.data.Organization;
import org.apache.commons.lang3.RandomUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

public class DataBuilder {

    private final AtomicLong employeeIdGenerator = new AtomicLong();
    private final AtomicLong organizationIdGenerator = new AtomicLong();
    private final AtomicLong departmentIdGenerator = new AtomicLong();
    private final Map<String, Organization> organizations = new HashMap<>();
    private final Map<String, Department> departments = new HashMap<>();

    public Employee employee() {
        return new Employee()
                .setId(employeeIdGenerator.incrementAndGet())
                .setName(randomAlphanumeric(5))
                .setAge(RandomUtils.nextInt(18, 100))
                .setPosition(randomAlphanumeric(10))
                .setDepartment(department())
                .setOrganization(organization());
    }

    public Department department() {
        Department department = new Department()
                .setId(departmentIdGenerator.incrementAndGet())
                .setName(randomAlphanumeric(3));
        departments.put(department.getName(), department);
        return department;
    }

    public Organization organization() {
        Organization organization = new Organization()
                .setId(organizationIdGenerator.incrementAndGet())
                .setName(randomAlphanumeric(4))
                .setAddress(randomAlphanumeric(15));
        organizations.put(organization.getName(), organization);
        return organization;
    }
}
