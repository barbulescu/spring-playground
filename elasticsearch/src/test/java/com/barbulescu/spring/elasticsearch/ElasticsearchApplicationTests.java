package com.barbulescu.spring.elasticsearch;

import com.barbulescu.spring.elasticsearch.data.Employee;
import com.barbulescu.spring.elasticsearch.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.query.FetchSourceFilter;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.data.util.CloseableIterator;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

@SpringBootTest
@Testcontainers
@ContextConfiguration(initializers = {ElasticsearchContextInitializer.class})
class ElasticsearchApplicationTests {

    @Autowired
    private ElasticsearchRestTemplate template;

    @Autowired
    private EmployeeRepository repository;

    private DataBuilder dataBuilder;

    @BeforeEach
    void before() {
        repository.deleteAll();
        dataBuilder = new DataBuilder();
    }

    @Test
    public void persistence() {
        Employee employee = dataBuilder.employee();

        repository.save(employee);
        Optional<Employee> response = repository.findById(employee.getId());
        assertThat(response).hasValue(employee);
    }

    @Test
    void findByName() {
        Employee employee1 = dataBuilder.employee();
        repository.save(employee1);
        Employee employee2 = dataBuilder.employee();
        repository.save(employee2);

        List<Employee> response = repository.findByName(employee1.getName());
        assertThat(response)
                .hasSize(1)
                .containsExactly(employee1);
    }

    @Test
    void findByOrganizationName() {
        Employee employee1 = dataBuilder.employee();
        repository.save(employee1);
        Employee employee2 = dataBuilder.employee();
        repository.save(employee2);

        String organizationName = employee1.getOrganization().getName();
        List<Employee> response = repository.findByOrganizationName(organizationName);
        assertThat(response)
                .hasSize(1)
                .containsExactly(employee1);
    }

    @Test
    void scroll() {
        createData();

        String[] includes = new String[]{"id", "name"};
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(matchAllQuery())
                .withSourceFilter(new FetchSourceFilter(includes, null))
                .build();

        AtomicInteger counter = new AtomicInteger();
        try (CloseableIterator<Employee> iterator = template.stream(searchQuery, Employee.class)) {
            while (iterator.hasNext()) {
                counter.incrementAndGet();
                iterator.next();
            }
        }
        assertThat(counter.get()).isEqualTo(100_000);
    }

    private void createData() {
        IntStream.range(0, 100_000)
                .mapToObj(it -> dataBuilder.employee())
                .forEach(it -> repository.save(it));

    }

}
