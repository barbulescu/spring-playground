package com.barbulescu.spring.elasticsearch;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

public class ElasticsearchContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    private static final ElasticsearchContainer container = new ElasticsearchContainer("docker.elastic.co/elasticsearch/elasticsearch:6.8.16");

    @Override
    public void initialize(ConfigurableApplicationContext context) {
        container.start();
        TestPropertyValues.of("spring.elasticsearch.rest.uris=" + container.getHttpHostAddress())
                .applyTo(context.getEnvironment());
    }
}
