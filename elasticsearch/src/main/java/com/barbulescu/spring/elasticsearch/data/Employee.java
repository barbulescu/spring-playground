package com.barbulescu.spring.elasticsearch.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.HashSet;
import java.util.Set;

@Document(indexName = "employee")
//@Mapping(mappingPath = "mappings/employee.json")
public class Employee {
    @Id
    private Long id;
    String name;
    private int age;
    private String position;
    @Field(type = FieldType.Object)
    private Department department;
    @Field(type = FieldType.Object)
    private Organization organization;
    @Field(type = FieldType.Text)
    private Set<String> tags = new HashSet<>();

    public Long getId() {
        return id;
    }

    public Employee setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Employee setName(String name) {
        this.name = name;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Employee setAge(int age) {
        this.age = age;
        return this;
    }

    public String getPosition() {
        return position;
    }

    public Employee setPosition(String position) {
        this.position = position;
        return this;
    }

    public Department getDepartment() {
        return department;
    }

    public Employee setDepartment(Department department) {
        this.department = department;
        return this;
    }

    public Organization getOrganization() {
        return organization;
    }

    public Employee setOrganization(Organization organization) {
        this.organization = organization;
        return this;
    }

    public Set<String> getTags() {
        return tags;
    }

    public Employee setTags(Set<String> tags) {
        this.tags = tags;
        return this;
    }
}
