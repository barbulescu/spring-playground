package com.barbulescu.spring.elasticsearch.data;

public class Organization {
    private Long id;
    private String name;
    private String address;

    public Long getId() {
        return id;
    }

    public Organization setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Organization setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Organization setAddress(String address) {
        this.address = address;
        return this;
    }
}
