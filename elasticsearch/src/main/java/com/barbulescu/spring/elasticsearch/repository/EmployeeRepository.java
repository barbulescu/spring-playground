package com.barbulescu.spring.elasticsearch.repository;

import com.barbulescu.spring.elasticsearch.data.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    List<Employee> findByOrganizationName(String name);
    List<Employee> findByName(String name);
}
