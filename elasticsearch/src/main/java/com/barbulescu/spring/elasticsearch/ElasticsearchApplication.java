package com.barbulescu.spring.elasticsearch;

import com.barbulescu.spring.elasticsearch.data.Employee;
import com.barbulescu.spring.elasticsearch.repository.EmployeeRepository;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;

import java.util.List;
import java.util.Set;

@SpringBootApplication
public class ElasticsearchApplication implements ApplicationRunner {

    public ElasticsearchApplication(EmployeeRepository repository, RestHighLevelClient client, ElasticsearchRestTemplate template) {
        this.repository = repository;
        this.client = client;
        this.template = template;
    }

    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchApplication.class, args);
    }

    private final EmployeeRepository repository;
    private final RestHighLevelClient client;
    private final ElasticsearchRestTemplate template;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        client.indices().delete(new DeleteIndexRequest("employee"), RequestOptions.DEFAULT);
        repository.deleteAll();
        Employee employee = new Employee()
                .setName("test")
                .setTags(Set.of("a1", "B1", "Cc"));
        repository.save(employee);

        SearchRequest searchRequest = new SearchRequest("employee");
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
        for (SearchHit hit : response.getHits()) {
            System.out.println(hit.getSourceAsString());
        }

        List<Employee> employees = template.queryForList(new NativeSearchQuery(QueryBuilders.termsQuery("tags", "qq", "CC", "ww")), Employee.class);
        for (Employee employee1 : employees) {
            System.out.println(employee1);
        }
    }
}
